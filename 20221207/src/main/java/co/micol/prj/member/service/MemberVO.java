package co.micol.prj.member.service;

import lombok.Getter;
import lombok.Setter;

//DTO 는 DAO의 값을 실어서 보냄. 
//@Data -> 어노스테이션 쓰면 자동으로 롬복이 개터새터 
//투스트링 만듬 (java에서는 투스트링도 필요하니까 걍 @Data 편함)
@Setter
@Getter //jsp 에게는 개별만 필요하니까 따로 만들어줌. 
public class MemberVO {
	private String memberId;
	private String memberName;
	private String memberPassword;
	private int memberAge;
	private String memberAddress;
	private String memberTel;
	private String memberAuthor;
		
	}
	
