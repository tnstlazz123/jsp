package co.micol.prj.member.map;

import java.util.List;

import co.micol.prj.member.service.MemberVO;

public interface MemberMapper {	
	List<MemberVO> memberSelectList(); //전체리스트(전체조회) 가져오기
	MemberVO memberSelect(MemberVO vo); // 한 사람 조회 또는 로그인
	int memberInsert(MemberVO vo); // 입력
	int memberDelete(MemberVO vo); // 삭제
	int memberUpdate(MemberVO vo); // 수정
	
	boolean isIdCheck(String id); // 아이디 중복체크 존재하면 false / 아이디 존재하면 false 변수만들때 디폴트 true;
	
}