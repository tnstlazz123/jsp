package co.micol.prj.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.micol.prj.common.Command;

public class MemberLogout implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 로그아웃 처리
		HttpSession session = request.getSession();
		String message = (String) session.getAttribute("name"); //오브젝트를 string으로 캐스캐이팅 해주느것.
		message += "님 정상적으로 로그아웃 처리되었다.";
		session.invalidate(); //세션을 통째로 없애버리는것.
		request.setAttribute("message", message);
		return "member/memberLogin.tiles";
	}

}
