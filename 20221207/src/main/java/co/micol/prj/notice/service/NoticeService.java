package co.micol.prj.notice.service;

import java.util.List;

public interface NoticeService {
	List<NoticeVO> noticeSelectList();
	//첨부파일여러개 있을수 있어서 단건조회지만 관계로 리스트로 받음
	List<NoticeVO> noticeSelect(); 
	NoticeVO noticeSelect(NoticeVO vo); //첨부파일 한개만 받음
	int noticeInsert(NoticeVO vo); //게시글 저장
	int noticeDelete(NoticeVO vo); //게시글 삭제
	int noticeUpdate(NoticeVO vo); //게시글 수정
	
	int noticeAttechInsert(NoticeAttechVO vo); //첨부파일 저장
	int noticeAttechDelete(NoticeAttechVO vo); //첨부파일 삭제
	
	List<NoticeVO> noticeSearchList(String key, String val); //게시글검색을 위해
	
	
	
	
}
