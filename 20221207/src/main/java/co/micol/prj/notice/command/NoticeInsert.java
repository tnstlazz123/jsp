package co.micol.prj.notice.command;
import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.FileRenamePolicy;

import co.micol.prj.common.Command;
import co.micol.prj.member.notice.serviceImpl.NoticeServiceImpl;
import co.micol.prj.notice.service.NoticeAttechVO;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;

public class NoticeInsert implements Command{

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 공지사항 글 등록하기 Muti PartRequest를 이용해야 한다. 
		NoticeService dao = new NoticeServiceImpl();
		NoticeVO vo = new NoticeVO();
		String saveDir = "attech" + File.separator;
		int size = 1024*1024*1024; //최대 100M까지 업로드
		
		MultipartRequest multi; //파일을 업로드 시 request 객체를 대체한다. 
		
		
		multi = new MultipartRequest(request, saveDir, 0, "utf-8", new FileRenamePolicy() {
			
			@Override
			public File rename(File arg0) {
				// TODO Auto-generated method stub
				return null;
			}
		})
		
		return null;
	}

}
