package co.micol.prj.notice.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import co.micol.prj.notice.service.NoticeAttechVO;
import co.micol.prj.notice.service.NoticeVO;

public interface NoticeMapper {
	List<NoticeVO> noticeSelectList();

	List<NoticeVO> noticeSelect(); //첨부파일 관계로 리스트로 받음
	List<NoticeVO> noticeSelect(); //첨부파일 한개만 받음
	
	
	int noticeInsert(NoticeVO vo); // 게시글 저장 

	int noticeDelete(NoticeVO vo); // 게시글 수정 

	int noticeUpdate(NoticeVO vo); // 게시글 삭제

	int noticeAttechInsert(NoticeAttechVO vo); // 첨부파일 저장

	int noticeAttechDelete(NoticeAttechVO vo); // 첨부파일 삭제

	List<NoticeVO> noticeSearchList(@Param("key") String key, @Param("val") String val);

}
