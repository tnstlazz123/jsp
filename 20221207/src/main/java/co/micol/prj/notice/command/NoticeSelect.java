package co.micol.prj.notice.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.micol.prj.common.Command;
import co.micol.prj.member.notice.serviceImpl.NoticeServiceImpl;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;

public class NoticeSelect implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		//  공지사항 상세보기
		NoticeService dao = new NoticeServiceImpl();
		List<NoticeVO> list = new ArrayList<NoticeVO>();
		NoticeVO vo = new NoticeVO();
		
	
		vo.setNoticeId(Integer.valueOf(request.getParameter("noticeId")));
		list = dao.noticeSelect(vo);
		request.setAttribute("notices", list);
		return "notice/noticeSelect.tiles";
	}

}
