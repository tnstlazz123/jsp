package co.micol.prj.common;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;


public class DataSource { //싱글톤 class
	private static SqlSessionFactory sqlSessionFactory;
	private DataSource() {}; //생성자란 내 클래스명과 동일한 메소드명. 생성자를 만들지 않으면 자바에서 퍼블릭으로 만듬.
	
	public static SqlSessionFactory getInstance() {
		String resource = "config/mybatis-config.xml";
		try {
			InputStream inputStream = Resources.getResourceAsStream(resource);
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		}catch(IOException e) {
			e.printStackTrace();
		}
		return sqlSessionFactory;
		
	}

}
