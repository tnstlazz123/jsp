package co.micol.prj.notice.command;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import co.micol.prj.common.Command;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;
import co.micol.prj.notice.serviceImpl.NoticeServiceImpl;

public class NoticeUpdate implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 공지사항 게시물 처리
		NoticeService dao = new NoticeServiceImpl();
		NoticeVO vo = new NoticeVO();
		
		String saveDir = request.getServletContext().getRealPath("attech/");
		int maxSize = 1024 * 1024 * 1024; 
		try {
			MultipartRequest multi = new MultipartRequest(request, saveDir, maxSize, "utf-8", new DefaultFileRenamePolicy());
			
			vo.setNoticeId(Integer.valueOf(multi.getParameter("noticeId")));
			vo.setNoticeDate(Date.valueOf(multi.getParameter("noticeDate")));
			vo.setNoticeTitle(multi.getParameter("noticeTitle"));
		    vo.setNoticeSubject(multi.getParameter("noticeSubject"));
		    
			request.setAttribute("notice", vo);
		    
			//이순간에 이미 저장
			String ofileName = multi.getOriginalFileName("nfile"); 
			String pfileName = multi.getFilesystemName("nfile"); 
			
			if(ofileName != null) {
				vo.setNoticeFile(ofileName);
				pfileName = saveDir + pfileName; 
				vo.setNoticeFileDir(pfileName);
				
			}
			
			int n = dao.noticeUpdate(vo);
			if(n != 0) {
				request.setAttribute("message", "공지사항이 수정되었습니다.");
			}else {
				request.setAttribute("message", "공지사항 수정실패했습니다.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "notice/noticeMessage.tiles";
	}

}
