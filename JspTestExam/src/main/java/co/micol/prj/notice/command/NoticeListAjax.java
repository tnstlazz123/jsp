package co.micol.prj.notice.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.micol.prj.common.Command;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;
import co.micol.prj.notice.serviceImpl.NoticeServiceImpl;

public class NoticeListAjax implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		//기존의 noticeList.javs(페이지 넘겨주는것.)
		//noticeListAjax
		//{"name":"홍길동","age":20} => json포멧
		//DB에서 가져온 데이터로 만들기
		NoticeService service = new NoticeServiceImpl();
		List<NoticeVO> list = service.noticeSelectList();
		ObjectMapper mapper = new ObjectMapper(); 
		
		String json = null;
		try {
			//list => JSON의 문자열(String)로 바꿔줌
			json = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
		return "Ajax:" + json;
	}


}
