package co.micol.prj.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.micol.prj.common.Command;

public class MemberLogout implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 로그아웃 처리 - 세션객체 없앰
		HttpSession session = request.getSession();
		
		String message = (String)session.getAttribute("name");
		message += "님 정상적으로 로그아웃 되었습니다.";
		//세션 완전히 삭제
		session.invalidate();
		//session.remove => id, 권한 등 하나만 없애고 싶을때
		request.setAttribute("message", message);
		return "member/memberLogin.tiles";
	}

}
