package co.micol.prj.member.service;

import java.util.List;

public interface MemberService {
	//전체조회(매개변수로는 보객체)
	List<MemberVO> memberSelectList();
	//한사람 조회 또는 로그인
	MemberVO memberSelect(MemberVO vo);
	//입력
	int memberInsert(MemberVO vo);
	//삭제
	int memberDelete(MemberVO vo);
	//수정
	int memberUpdate(MemberVO vo);
	
	//회원가입시 아이디 중복체크 (메소드 만들때 불린타입 이름 is~) 
	//아이디 존재하면 false; 변수 만들때 디폴트는 true;
	boolean isIdCheck(String id);
	

}
