package co.micol.prj.member.map;

import java.util.List;

import co.micol.prj.member.service.MemberVO;

public interface MemberMapper { //xml에서 쓸거
		List<MemberVO> memberSelectList();
		MemberVO memberSelect(MemberVO vo);
		int memberInsert(MemberVO vo);
		int memberDelete(MemberVO vo);
		int memberUpdate(MemberVO vo);
		

		boolean isIdCheck(String id);
}
