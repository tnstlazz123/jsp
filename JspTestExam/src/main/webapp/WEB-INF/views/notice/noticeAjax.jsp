<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>noticeAjax.jsp</title>
	<script src="js/table.js"></script>
</head>

<body>
	<h3>Ajax연습.</h3>

	<div>
		<table>

			<form name="ajaxFrm" action="">
				<tr>
					<th><label for="writer">작성자</label></th>
					<td><input type="text" name="writer" id="writer"></td>
				</tr>
				</tr>
				<th><label for="title">제목</label></th>
				<td><input type="text" name="title" id="title"></td>
				<tr>
					<th><label for="subject">내용</label></th>
					<td><textarea cols="30" rows="3" name="subject" id="subject"></textarea></td>
				</tr>
				<th>
				<td colspan="2">
					<input type="submit" value="저장">
				</td>
				</th>
			</form>
		</table>
	</div>


	<div id="show"></div>
	<!-- 여기서 서버로 바로 데이터 요청함 -->
	<script>
		const xhtp = new XMLHttpRequest(); // 객체 호출
		xhtp.open('GET', 'noticeListAjax.do'); // 링크는 프론트컨트롤러의 이름과 같아야함(복붙하자!)
		xhtp.send();
		xhtp.onload = function () {
			let data = JSON.parse(xhtp.response); // json(문자열타입) -> js의 object로 변환
			console.log(data);

			table.initData = data;
			table.showField = ['noticeId', 'noticeWriter', 'noticeTitle', 'noticeDate', 'noticeHit', 'noticeFile'];
			let tbl = table.makeTable(); // table,thead,tbody
			// thead -> 타이틀추가.
			// table.addTitle = function(){
			// 	let th = document.createElement('th');
			// 	th.innerText = '삭제';
			// 	console.log(table.thead);
			// }
			// tbody -> 버튼 추가
			//table.addTitle('삭제'); 내일

			document.getElementById('show').append(tbl);
		}

		// 입력처리.
		document.querySelector('form[name=ajaxFrm]')
			.addEventListener('submit', addNotice);

		function addNotice(e){
			e.preventDefault(); //submit의 기능 차단.
			let writer = document.getElementById('writer').value;
			let title = document.getElementById('title').value;
			let subject = document.getElementById('subject').value;
			const xhtp = new XMLHttpRequest();
			xhtp.open('get', 'noticeAddAjax.do?writer='+writer+'&title='+title+'&subject='+subject+'&noticeDate=2022-12-13'); // get:
			xhtp.send();
			xhtp.onload = function(){
				let data = JSON.parse(xhtp.response);
				console.log(data);


			}

		}
	</script>
</body>

</html>