// table.js
// export const table = {
const table = { // export : 다른 js에서도 import해서 사용가능
//    initData: [{name:"홍길동",age:20},{name:"김민수",age:22}],
	initData: [],
	showField: [],
    makeTable: function(){
        this.table = document.createElement('table'); // this붙이면 다른 함수에서도 사용가능(this는 객체자신을 가리킴)\
        this.table.setAttribute('border','1')
        this.makeHead();
        this.makeBody();
        return this.table;
    },
    makeHead: function(){
        this.thead = document.createElement('thead'); // this쓰면 필드 생성됨
        this.htr = document.createElement('tr');
        let fields = this.showField; // 꼭 디스로 가져와야.
        for(let prop of fields){
            let th = document.createElement('th');
            th.innerText = prop.toUpperCase(); // 헤더쪽에 넣을거라 대문자로 바꿔서 th에 넣어줌
            this.htr.append(th);
        }
        this.thead.append(this.htr);
        this.table.append(this.thead);
    },
	addTitle: function(title){
		let th = document.createElement('th');
		th.innerText = title;
		this.htr.append(th);
	},
    makeBody: function(){
        let tbody = document.createElement('tbody'); // 변수에 담아놓음
		let sfield = this.showField;
        this.initData.forEach((item) => {
            //item: {name:"홍길동",age:20}
            let tr = document.createElement('tr');
            for(let prop of sfield){
                let td = document.createElement('td');
                td.innerText = item[prop];
                tr.append(td);
            }
            tbody.append(tr);
            // this.tbody.append(tr); // tbody가 없다는 오류 : 함수 밖에서의 this는 table이 아니라 window라서.
        });
        // this.tbody = tbody;
        this.table.append(tbody); // 
    },
    makeTr: function(){

    }
}
// export default table;