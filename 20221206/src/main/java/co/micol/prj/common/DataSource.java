package co.micol.prj.common;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class DataSource {

	// 내가 돌려줄 리턴 값
	private static SqlSessionFactory sqlSessionFactory;

	// 외부에서 자기자신을 생성하지 못하도록 프라이빗 생성자 미리 만들기. DataSource가 인터페이스
	private DataSource() {};

	// 인스턴스를 이용해 가져갈수 있도록.
	public static SqlSessionFactory getInstance() {
		String resource = "config/mybatis-config.xml";
		try {
			InputStream inputStream = Resources.getResourceAsStream(resource);
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sqlSessionFactory;
	}

}
