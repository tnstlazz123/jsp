package co.micol.prj.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {
	//request,response 값을 받아서 결과를 스트링으로 돌려주는 메소드 
	String exec(HttpServletRequest request, HttpServletResponse response);
}
