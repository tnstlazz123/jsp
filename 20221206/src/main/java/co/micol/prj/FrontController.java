package co.micol.prj;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.micol.prj.common.Command;
import co.micol.prj.member.command.MemberList;

@WebServlet("*.do")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HashMap<String, Command> map = new HashMap<String, Command>();

	public FrontController() {
		super();

	}

	public void init(ServletConfig config) throws ServletException {
		// 명령(Command)를 저장하는 영역
		map.put("/main.do", new MainCommand()); //실제요청한 페이지가 무엇인지 확인. / 처음페이지 명령.
		map.put("/memberList.do", new MemberList()); //멤버 목록보기
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Controller 본체
		request.setCharacterEncoding("utf-8"); // 한글깨짐 방지를 위해
		 
		String uri = request.getRequestURI(); // uri 값을 읽어온다.
		String contextPath = request.getContextPath(); // ContextPath를 읽어온다.
		String page = uri.substring(contextPath.length()); // 실제요청명을 구한다.

		Command command = map.get(page); // 수행할 command를 찾고
		String viewPage = command.exec(request, response); // 찾은 command를 실행.
		// command가 동작이 되면 나에게 돌려줄 페이지를 찾는다.

		if (!viewPage.endsWith(".do")) {
			//Ajax 처리하는 루틴
			viewPage = "WEB-INF/views/" + viewPage + ".jsp";

			RequestDispatcher dispatcher = request.getRequestDispatcher(viewPage); // 내가던진 리퀘스트가 최종까지 감.
			dispatcher.forward(request, response);
		} else {
			response.sendRedirect(viewPage); // 나의 요청은 무시하고 새로운 페이지로 가는것.

		}

	}
	
}